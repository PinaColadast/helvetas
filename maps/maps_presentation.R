require(sf)
require(dplyr)
require(ggplot2)

#read data
shp = read_sf("map_data.shp") # you can add more info joining by municipality number (mun_number)

# create donors per capita column
shp$donors_pc = shp$count/shp$pop

# remove weird observations
percentile = quantile(shp$donors_pc, probs = seq(0, 1, by= 0.025), na.rm = TRUE)[38] # 95% percentile
shp$donors_pc[shp$donors_pc > percentile] = NA

# DONORS PER CAPITA
p_donors_pc  = ggplot(data = shp) +
  geom_sf(aes(fill = donors_pc), size = 0.1) +
  scale_fill_viridis_c(option = "inferno", trans = "sqrt") +
  labs(fill = "donors p.c") + 
  theme_minimal()
  
ggsave("donors_pc.png", width = 14, height = 9, plot = p_donors_pc)

# INCOME PC
p_income_pc  = ggplot(data = shp) +
  geom_sf(aes(fill = income_pc), size = 0.1) +
  scale_fill_viridis_c(option = "inferno", trans = "sqrt") +
  labs(fill = "income p.c") + 
  theme_minimal()

ggsave("income_pc.png", width = 14, height = 9, plot = p_income_pc)

# AGE PER MUNICIPALITY
p_age  = ggplot(data = shp) +
  geom_sf(aes(fill = year), size = 0.1) +
  scale_fill_viridis_c(option = "inferno", trans = "sqrt") +
  labs(fill = "year") + 
  theme_minimal()

ggsave("age_mun.png", width = 14, height = 9, plot = p_age)
