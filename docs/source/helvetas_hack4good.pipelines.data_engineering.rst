Data Engineering
=======================================================

.. automodule:: helvetas_hack4good.pipelines.data_engineering
    :members:
    :undoc-members:
    :show-inheritance:


Data Engineering Nodes
------------------------------------------------------------

.. automodule:: helvetas_hack4good.pipelines.data_engineering.nodes
    :members:
    :undoc-members:
    :show-inheritance:

