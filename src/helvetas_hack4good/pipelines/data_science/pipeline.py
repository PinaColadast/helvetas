
"""
Data Science Pipeline
"""

from kedro.pipeline import Pipeline, node
from .nodes import feature_selection, split_data, train_model, predict_test, predict_unlabeled


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=feature_selection,
                inputs=["features_data", "params:feat_selection", "params:feat_list", "params:k", "params:verbose"],
                outputs="model_input_data",
                name="selecting_features"
            ),
            node(
                func=split_data,
                inputs=["model_input_data", "params:test_size",  "params:column_to_predict", "params:seed", "params:verbose"],
                outputs=["unlabeled_data", "labeled_data", "train_data", "test_data"],
                name="splitting_data"
            ),
            node(
                func=train_model,
                inputs=["train_data", "params:model_name", "params:column_to_predict", "params:seed",
                        "params:standardize", "params:verbose", "params:epochs", "params:validation_split"],
                outputs="model",
                name="training_model"
            ),
            node(
                func=predict_test,
                inputs=["model", "params:model_name", "test_data", "params:column_to_predict"],
                outputs=["test_prediction", "model_metrics"],
                name="predicting_test"
            ),
            node(
                func=predict_unlabeled,
                inputs=["model", "params:model_name", "unlabeled_data"],
                outputs="prediction_data",
                name="predicting_unlabeled"
            ),
        ], tags="data_science"
    )
