"""
Data Science nodes
"""
# pylint: disable=invalid-name
import os
import time
from datetime import datetime
import pandas as pd
import numpy as np

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNetCV, LassoCV
from sklearn.pipeline import make_pipeline
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor
from sklearn.dummy import DummyRegressor
from sklearn.feature_selection import SelectKBest, f_regression

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.layers.experimental import preprocessing


def feature_selection(data: pd.DataFrame, feat_selection: str, feat_list: list, k: int, verbose:bool) -> pd.DataFrame:
    """
    Basic feature selection by variable source and user defined.

    Parameters
    ----------
    data : pd.DataFrame
        The dataframe with all  features.
    feat_selection : string
        Feature selection method.
    feat_list: list
        Features names for custom selection.
    k: int
        Number of features to select when selecting the highest score features by univariates regression
    verbose : bool, optional
        Whether to print selected feature names
    Returns
    -------
    pd.DataFrame
        Data to be used in the model.
    """
    data = data.copy(deep=True)

    helvetas_feat = ["sprachcd_" + str(i) for i in range(1, 8)] + ["gescode_" + str(i) for i in range(1, 4)] + ["quelle_cat_" + str(i) for i in range(1, 7)] + ["kanton_" + str(i) for i in range(1, 31)] + ['log_qty_donations', 'log_LTV_donations', 'AVG_donation', 'active_member', 'has_been_member',
                                                                                                                                                                                                             'num_andat', 'num_first_donation', 'num_last_donation', 'gebjah']

    if feat_selection == "all":
        feats = data.columns

    elif feat_selection == "only_helvetas":
        feats = helvetas_feat

    elif feat_selection == "demographics":
        demographics_feat = ["region_" + str(i) for i in range(1, 9)] + ['E', 'N', 'municipality_pop', 'municipality_year', 'municipality_maxyear',
                                                                         'district_pop', 'kanton_pop', 'district_year', 'kanton_year', 'district_maxyear', 'kanton_maxyear', 'mun_n_taxpayers',
                                                                         'mun_income_kCHF', 'kan_n_taxpayer', 'kan_wealth_Mchf', 'dis_income_kCHF', 'dis_n_taxpayers', 'kan_income_kCHF',
                                                                         'mun_income_kCHF_ptp', 'mun_income_kCHF_pc', 'dis_income_kCHF_ptp', 'dis_income_kCHF_pc', 'kan_income_kCHF_ptp',
                                                                         'kan_income_kCHF_pc', 'kan_wealth_Mchf_ptp', 'kan_wealth_Mchf_pc', 'ed0', 'ed1', 'ed2', 'ed3', 'ed4']
       # Log features
       # ['log_district_pop', 'log_mun_n_taxpayers','log_mun_income_kCHF', 'log_kanton_pop', 'log_kan_n_taxpayer', 'log_kan_wealth_Mchf', 'log_dis_income_kCHF',
       # 'log_dis_n_taxpayers', 'log_kan_income_kCHF', 'log_mun_income_kCHF_ptp', 'log_mun_income_kCHF_pc', 'log_dis_income_kCHF_ptp',
       # 'log_dis_income_kCHF_pc', 'log_kan_income_kCHF_ptp', 'log_kan_income_kCHF_pc', 'log_kan_wealth_Mchf_ptp', 'log_kan_wealth_Mchf_pc']
        feats = helvetas_feat + demographics_feat

    elif feat_selection == "demographics2":
        demographics_feat = ["region_" + str(i) for i in range(1, 9)] + ['E', 'N', 'municipality_pop', 'municipality_year', 'municipality_maxyear',
                                                                         'mun_income_kCHF']
        feats = helvetas_feat + demographics_feat

    elif feat_selection == "names":
        names_feat = ['vname_year', 'vname_maxyear', 'gen1', 'gen2', 'gen3', 'gen4', 'gen5', 'gen6']
        feats = helvetas_feat + names_feat

    elif feat_selection == "kbest":
        label = ["gebjah", "num_first_donation"]
        X = data.drop(label, axis=1)
        print(data)
        y = data[label]
        selector = SelectKBest(f_regression, k=k)
        X_new = selector.fit_transform(X, data[["gebjah"]])
        cols = selector.get_support(indices=True)
        features = X.columns[cols]
        data = pd.DataFrame(X_new, index=X.index, columns=features)
        data = data.merge(y, left_index=True, right_index=True)
        feats = data.columns
        print(data)

    elif feat_selection == "custom":
        feats = feat_list + ['gebjah']

    elif feat_selection == "from_dataset":
        from_dataset = pd.read_pickle("././././data/05_model_input/model_input_data.pkl")
        feats = from_dataset.columns
        
    data = data[feats]

    if verbose: 
            print("Selected best features:")
            print(feats)

    return data


def relabel_data(data: pd.DataFrame, verbose=True) -> pd.DataFrame:
    """
    Creates the label 'label_age_at_acquisition'

    Parameters
    ----------
    data : pd.DataFrame
        The data frame to enhance data with. Needs to contain 'gebjah', 'first_donation', 'last_donation'

    Returns
    -------
    pd.DataFrame
        The data with 3 more columns.
    """
    # Add label_age_at_acquisition
    # data            = data[data['gebjah']!=0] #This is done later with the split_data function
    data['tmp'] = pd.to_datetime("06-15", format="%m-%d")
    data['birthdate'] = pd.to_datetime(pd.DataFrame(
        {'year': data.gebjah, 'month': data.tmp.dt.month, 'day': data.tmp.dt.day}))
    data = map_datetime_to_decimal(data, 'birthdate', inplace=True)
    data['label_age_at_acquisition'] = data['num_first_donation'] - data['birthdate']

    # Clean-up
    data.drop({'tmp', 'birthdate'}, inplace=True, axis=1)

    return data


def split_data(data: pd.DataFrame, test_size: float, column_to_predict: str, seed: int, verbose: bool) -> [pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """
    Takes in the full dataset and splits it into unlabeled (without birth year) dataset and labeled (with a birth year).
    Then, it splits the labeled dataset into a train and test datasets according to the user specified parameter test_size.

    Parameters
    ----------
    data : pd.DataFrame
        The data frame to split.
    column_to_predict: str
        The label to predict: gebjah or label_age_at_acquisition
    verbose : bool, optional
        Whether to print information about the length of the splitted data frames, by default True.

    Returns
    -------
    Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]
        The labeled and the unlabeled data, the train and test set.
    """

    labeled_data = data[data['gebjah'] != 0].copy()
    unlabeled_data = data[data['gebjah'] == 0].copy().drop('gebjah', axis='columns')

    if verbose:
        print("{:40}: {:d}".format("Number of labeled entries with birth year", len(labeled_data.index)))
        print("{:40}: {:d}".format("Number of unlabeled entries without birth year", len(unlabeled_data.index)))

    # Create new label 'label_age_at_acquisition'
    labeled_data = relabel_data(labeled_data)

    train_data, test_data = train_test_split(labeled_data, test_size=test_size, random_state=seed)

    return unlabeled_data, labeled_data, train_data, test_data


def train_model(train: pd.DataFrame, model_name: str, column_to_predict: str, seed: int, standardize: bool, verbose: bool, epochs: int, validation_split: float) -> np.ndarray:
    """
    Training the model specified in parameters with the train dataset.

    Parameters
    ----------
    train : pd.DataFrame
        Train dataset.
    model_name: str
        The model to train.
    column_to_predict: str
        The label to predict: gebjah or label_age_at_acquisition
    standardize: bool
        Parameter for neural network model.
    epochs: int
        Parameter for neural network model.
    validation_split: float
        Parameter for neural network model.
    seed: int
    verbose : bool, optional
        Whether to print information about the length of the splitted data frames, by default True.
    
    Returns
    -------
    Trained model
    """
    if column_to_predict == 'label_age_at_acquisition':
        # The predicted column is "label_age_at_acquisition" which is a scalar from roughly [15-100], and it is strongly dependent on the year of birth.
        # As both tend to be unknowns to us / we try to predict them, and as they are dependent, we have to remove them together.
        train.drop(["gebjah"], axis=1, inplace=True)
    elif column_to_predict == 'gebjah':
        train.drop(["label_age_at_acquisition"], axis=1, inplace=True)

    train_x = train.drop([column_to_predict], axis=1)
    train_y = train[[column_to_predict]]

    if model_name == "baseline":
        model = DummyRegressor(strategy="mean")
        model.fit(train_x, train_y)
    elif model_name == "elastic_net":
        model = elastic_net(train_x, train_y, seed)
    elif model_name == "lasso":
        model = lasso(train_x, train_y, seed)
    elif model_name == "neural_net":
        model = neural_net(train_x, train_y, standardize, verbose, epochs, validation_split)
    elif model_name == "RandomForest":
        model = RandomForest(train_x, train_y, column_to_predict, seed, verbose)

    return model


def predict_test(model, model_name: str, test: pd.DataFrame, column_to_predict: str):
    """
    Predict the test dataset and compute evaluation metrics.

    Parameters
    ----------
    model : np.array
        Trained model.
    model_name: str
        The model name.
    test: pd.DataFrame
        Test dataset.
    column_to_predict: str
        The label to predict: gebjah or label_age_at_acquisition

    Returns
    -------
    Predictions for test dataset and evaluation metrics.
    """
    if column_to_predict == 'label_age_at_acquisition':
        # The predicted column is "label_age_at_acquisition" which is a scalar from roughly [15-100], and it is strongly dependent on the year of birth.
        # As both tend to be unknowns to us / we try to predict them, and as they are dependent, we have to remove them together.
        test.drop(["gebjah"], axis=1, inplace=True)
    if column_to_predict == 'gebjah':
        test.drop(["label_age_at_acquisition"], axis=1, inplace=True)

    test_x = test.drop([column_to_predict], axis=1)
    test_y = test[[column_to_predict]]

    if model_name == "neural_net":
        test_x = test_x.astype('float32')
        test_y = test_y.astype('float32')

    predict_y = model.predict(test_x)
    metrics = eval_metrics(test_y, predict_y)

    return predict_y, metrics


def predict_unlabeled(model, model_name: str, unlabeled_data: pd.DataFrame):
    """
    Predict the unlabeled dataset.

    Parameters
    ----------
    model : np.array
        Trained model.
    model_name: str
        The model name.
    unlabeled_data: pd.DataFrame
        The unlabeled dataset.

    Returns
    -------
    Predictions for unlabeled dataset.
    """
    predictions = pd.DataFrame(data=model.predict(unlabeled_data), index=unlabeled_data.index)
    predictions.sort_index(inplace=True)
    predictions.reset_index(inplace=True)
    predictions.columns = ['adrnum', 'gebjah']

    return predictions


# Models
def elastic_net(train_x: pd.DataFrame, train_y: pd.DataFrame, seed: int) -> np.ndarray:
    # Data is not standardized so we have to set fit_intercept and normalize to True
    model = ElasticNetCV(cv=5, fit_intercept=True, normalize=True, random_state=seed)
    model.fit(train_x, train_y)

    return model


def lasso(train_x: pd.DataFrame, train_y: pd.DataFrame, seed: int) -> np.ndarray:
    # Data is not standardized so we have to set fit_intercept and normalize to True
    model = LassoCV(n_alphas=1000, cv=10, fit_intercept=True, normalize=True, max_iter=10000, random_state=seed)
    model.fit(train_x, train_y)

    return model


def neural_net(train_x: pd.DataFrame, train_y: pd.DataFrame, standardize: bool, verbose: bool, epochs: int, validation_split: float) -> np.ndarray:

    # convert to float for tensor
    train_x = train_x.astype('float32')
    train_y = train_y.astype('float32')

    if standardize:
        train_x = preprocessing.scale(train_x)

    # NN model
    model = Sequential([
        Dense(32, input_dim=train_x.shape[1], activation='relu'),
        Dense(32, activation='relu'),
        # Dropout(0.3), # dropout
        Dense(10, activation='relu'),
        Dense(1)
    ])

    # compile te model
    model.compile(loss='mean_absolute_error',
                  optimizer=tf.keras.optimizers.Adam(learning_rate=0.1))
    # fit the model
    model.fit(
        train_x, train_y,
        validation_split=validation_split,
        verbose=verbose,
        epochs=epochs)

    return model


def RandomForest(train_x: pd.DataFrame, train_y: pd.DataFrame, column_to_predict: str, seed: int, verbose: bool) -> np.ndarray:
    """
    RandomForest regressor, fine-tuned
    """
    X_train, y_train = np.array(train_x), np.array(train_y)

    if column_to_predict == "gebjah":
        regr_rf = RandomForestRegressor(n_estimators=250,
                                        max_depth=38,
                                        min_samples_split=5,
                                        min_samples_leaf=1,
                                        max_features="sqrt",
                                        random_state=seed,
                                        criterion="mse",
                                        bootstrap=True
                                        )

        regr_rf.fit(X_train, y_train)
    elif column_to_predict == "label_age_at_acquisition":
        regr_rf = RandomForestRegressor(n_estimators=250,
                                        max_depth=80,
                                        min_samples_split=2,
                                        min_samples_leaf=1,
                                        max_features="sqrt",
                                        random_state=seed,
                                        criterion="mse",
                                        bootstrap=True)
        regr_rf.fit(X_train, y_train)

    if verbose:
        print("Feature ranking:")
        for f in range(X_train.shape[1]):
            print(train_x.columns[f], ": ", regr_rf.feature_importances_[f])

    return regr_rf


# Helper functions
###################

def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)

    metrics = {"rmse": {"value": rmse, "step": 1},
               "r2": {"value": r2, "step": 1},
               "mae": {"value": mae, "step": 1},
               }

    return metrics


def map_datetime_to_decimal(data, datetime_column, inplace=False):
    # Convert datetime columns to decimal columbs
    if inplace:
        data[datetime_column] = data[datetime_column].map(datetime2decimal)
    else:
        data['num_{}'.format(datetime_column)] = data[datetime_column].map(datetime2decimal)
    return data


def datetime2decimal(date):
    """
    Converts dates to decimal numbers

    Parameters
    ----------
    date : datetime

    Returns
    -------
    Float64
        Decimal representation of the date.
    """
    if pd.isna(date):
        return date
    year_part = date - datetime(year=date.year, month=1, day=1)
    year_length = (datetime(year=date.year + 1, month=1, day=1) - datetime(year=date.year, month=1, day=1))
    return date.year + year_part / year_length
