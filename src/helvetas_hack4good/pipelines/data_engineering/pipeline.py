"""
Data engineering pipeline definition
"""

from kedro.pipeline import Pipeline, node
#from .nodes import clean_data, split_label, enhance_data
from .nodes import clean_helvetas_data, clean_external_data, transform_external_data, enhance_data, create_features


def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=clean_helvetas_data,
                inputs=["helvetas_data", "params:verbose"],
                outputs="preprocessed_helvetas",
                name="cleaning_helvetas_data"
            ),
            node(
                func=clean_external_data,
                inputs=["swiss_plz", "swiss_geo", "municipality_population_data",
                        "income_data", "wealth_data", "education_data",
                        "male_names", "female_names", "params:verbose"],
                outputs=["preprocessed_postcodes", "preprocessed_populations", "preprocessed_incomes",
                         "preprocessed_wealths", "preprocessed_education", "preprocessed_names"],
                name="cleaning_external_data"
            ),
            node(
                func=transform_external_data,
                inputs=["preprocessed_postcodes", "preprocessed_populations", "preprocessed_incomes",
                        "preprocessed_wealths", "preprocessed_education", "preprocessed_names", "params:verbose"],
                outputs=["transformed_demographics", "transformed_names"],
                name="transforming_external_data"
            ),
            node(
                func=enhance_data,
                inputs=["preprocessed_helvetas", "transformed_demographics", "transformed_names", "params:verbose"],
                outputs="enhanced_data",
                name="enhancing_data"
            ),
            node(
                func=create_features,
                inputs=["enhanced_data", "params:encoding", "params:missing_action"],
                outputs="features_data",
                name="creating_features"
            )
        ], tags="data_engineering"
    )
